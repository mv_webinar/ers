<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<%@ page import="com.revature.beans.ErsRequest"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/lightbox.css" rel="stylesheet">
<script src="js/sorttable.js"></script>
<title>manager home</title>
</head>
<body>
	<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="managerHome.jsp"> <img alt="Brand"
				src="images/companyName.png" style="height: 63px; width: 121px"></a>
		</div>

		<p class="navbar-text navbar-right">
			current user: <a href="#" class="navbar-link">${user.username}</a>
		</p>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="active"><a href="managerHome.jsp">home <span
						class="sr-only">(current)</span></a></li>
				<li class="dropdown"><a name="dropdown" href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">filter by: <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a name="femploy" href="filterEmployee.jsp">employee</a></li>
						<li><a href="filterPending">all pending</a></li>
						<li><a href="filterResolved">all resolved</a></li>
					</ul></li>
			</ul>
			<ul class="nav navbar-nav">
				<li><a name="logout" href="logoutSubmit">log out</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid --> </nav>

	<div
		style="background-color: #f8f8f8; text-align: center; font-size: 16px">
		<b>employees</b>
	</div>
	<table class="table table-condensed table-hover table-responsive">
		<thead>
			<tr>
				<th>uid</th>
				<th>username</th>
				<th>first name</th>
				<th>last name</th>
				<th>email</th>
			</tr>
		</thead>
		<tbody id="employeeTable">
			<!-- populate table with employee JSON -->
		</tbody>
	</table>
	<div class="col-sm-offset-2 col-sm-10">
		<a href="registerEmployee.jsp">
			<button id="regEmployee" name="regEmployee" type="button"
				class="btn btn-default">register employee</button>
		</a>
	</div>

	<br>
	<br>

	<div
		style="background-color: #f8f8f8; text-align: center; font-size: 16px">
		<b>requests</b>
	</div>
	<table
		class="table table-condensed table-hover table-responsive sortable">
		<tr>
			<th>id</th>
			<th>amount</th>
			<th>description</th>
			<th>submitted</th>
			<th>resolved</th>
			<th>employee</th>
			<th>resolver</th>
			<th>type</th>
			<th>status</th>
		</tr>
		<c:forEach items="${requests}" var="request">
			<tr>
				<td><c:out value="${request.rid}"></c:out></td>
				<td>$<c:out value="${request.amount}"></c:out></td>
				<td><c:out value="${request.description}"></c:out></td>
				<td><c:out value="${request.submitted}"></c:out></td>
				<td><c:out value="${request.resolved}"></c:out></td>
				<td><c:out value="${request.author}"></c:out></td>
				<td><c:out value="${request.resolver}"></c:out></td>
				<td><c:out value="${request.type}"></c:out></td>
				<td><c:out value="${request.status}"></c:out></td>
				<td><a href='data:image/jpg;base64, ${request.receipt}'
					data-lightbox='image-1'>
						<button type='button' class='btn btn-default'>view
							receipt</button>
				</a></td>
				<td><form action="resolveReq" method="post">
						<input type="hidden" name="rstatus" value="1" /> <input
							type="hidden" name="rid" value="${request.rid}" /> <input
							type="hidden" name="eid" value="${request.uidAuthor}" />
						<button type='submit' class='btn btn-default'>approve</button>
					</form></td>
				<td><form action="resolveReq" method="post">
						<input type="hidden" name="rstatus" value="2" /> <input
							type="hidden" name="rid" value="${request.rid}" /> <input
							type="hidden" name="eid" value="${request.uidAuthor}" />
						<button type='submit' class='btn btn-default'>deny</button>
					</form></td>
			</tr>
		</c:forEach>
	</table>

	<script type="text/javascript">
		$
				.get(
						'EmployeeJSONServlet',
						function(responseJson) {
							if (responseJson != null) {
								$("#employeeTable").find("tr:gt(0)").remove();
								var table1 = $("#employeeTable");
								$
										.each(
												responseJson,
												function(key, value) {
													var rowNew = $("<tr><td></td><td></td><td></td><td></td><td></td></tr>");
													rowNew.children().eq(0)
															.text(value['uid']);
													rowNew
															.children()
															.eq(1)
															.text(
																	value['username']);
													rowNew
															.children()
															.eq(2)
															.text(
																	value['fname']);
													rowNew
															.children()
															.eq(3)
															.text(
																	value['lname']);
													rowNew
															.children()
															.eq(4)
															.text(
																	value['email']);
													rowNew.appendTo(table1);
												});
							}
						});
	</script>
	<script src="js/lightbox.js"></script>
</body>
</html>