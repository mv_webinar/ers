<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>register employee</title>
</head>
<body>
	<div
		style="background-color: #f8f8f8; text-align: center; font-size: 16px">
		<b>register an employee</b>
	</div>
	<form action="registerEmployee" method="post">
		<div class="form-group">
			<div class="col-sm-10">
				<label for="username">username:</label> <input type="text"
					name="username" class="form-control" id="username">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10">
				<label for="fname">first name:</label> <input type="text"
					name="fname" class="form-control" id="fname">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10">
				<label for="lname">last name:</label> <input type="text"
					name="lname" class="form-control" id="lname">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10">
				<label for="email">email:</label> <input type="email" name="email"
					class="form-control" id="email">
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button name="register" type="submit" class="btn btn-default">register</button>
			</div>
		</div>
	</form>
</body>
</html>