<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<%@ page import="com.revature.beans.ErsRequest"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/lightbox.css" rel="stylesheet">
<title>filter by: employee</title>
</head>
<body>

	<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="managerHome.jsp"> <img alt="Brand"
				src="images/companyName.png" style="height: 63px; width: 121px"></a>
		</div>

		<p class="navbar-text navbar-right">
			current user: <a href="#" class="navbar-link">${user.username}</a>
		</p>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="managerHome.jsp">home</a></li>
				<li class="dropdown"><a name="dropdown" href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">filter by: <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li class="active"><a href="filterEmployee.jsp">employee</a></li>
						<li><a name="fpend" href="filterPending">all pending</a></li>
						<li><a href="filterResolved">all resolved</a></li>
					</ul></li>
			</ul>
			<ul class="nav navbar-nav">
				<li><a href="logoutSubmit">log out</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid --> </nav>

	<div style="background-color: #f8f8f8; font-size: 16px">
		<b>viewing requests from:</b>
	</div>
	<div class="form-group">
		<div class="col-sm-12">
			<select id="empList" name="empList" class="form-control"
				id="inputExpenseType">
				<option id="appendEmp" value="" selected disabled hidden>select
					an employee...</option>
			</select>
		</div>
	</div>

	<br>
	<br>
	<br>
	<table class="table table-condensed table-hover table-responsive"
		id="employee">
		<!-- rows of reimbursements from employee shown here -->
	</table>

	<iframe name="goHere" class="hide"></iframe>

	<script>
		//function to populate dropdown menu with list of employees
		function appendEmp(username, uid) {
			$("#appendEmp").after(
					$("<option></option>").text(username).val(uid));
		};

		//fill table according to dropdown selection
		$(document).ready(function() {
			$('#empList').change(function(event) {
				var emp = $('#empList').val();
				$.get('EfilterServlet', {
					uid : emp
				}, function(responseText) {
					$('#employee').html(responseText);
				});
			});
		});
	</script>

	<c:forEach items="${employees}" var="employee">
		<script>
		<!-- populate dropdown menu with list of employees -->
			appendEmp('${employee.username}', '${employee.uid}')
		</script>
	</c:forEach>

	<script src="js/lightbox.js"></script>
</body>
</html>