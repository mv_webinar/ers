<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<%@ page import="com.revature.beans.ErsRequest"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="js/hideShowPassword.min.js"></script>
<title>account info</title>
</head>
<body>

	<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="employeeHome.jsp"> <img alt="Brand"
				src="images/companyName.png" style="height: 63px; width: 121px"></a>
		</div>

		<p class="navbar-text navbar-right">
			current user: <a href="#" class="navbar-link">${user.username}</a>
		</p>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="employeeHome.jsp">home</a></li>
				<li><a href="employeeReq.jsp">submit a request</a></li>
				<li class="active"><a href="employeeInfo.jsp">account</a></li>
				<li><a href="logoutSubmit">log out</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid --> </nav>

	<div
		style="background-color: #f8f8f8; text-align: center; font-size: 16px">
		<b>update information page</b>
	</div>
	<form action="updateSubmit" method="post">
		<div class="form-group">
			<div class="col-sm-10">
				<label for="fname">first name:</label> <input type="text"
					name="fname" class="form-control" id="fname" value="${user.fname}">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10">
				<label for="lname">last name:</label> <input type="text"
					name="lname" class="form-control" id="lname" value="${user.lname}">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10">
				<label for="email">email:</label> <input type="email" name="email"
					class="form-control" id="email" value="${user.email}">
			</div>
		</div>


		<div class="form-group">
			<div class="col-sm-10">
				<label for="password">password:</label> <input type="password"
					name="password" class="form-control" id="password"
					placeholder="enter new password...">
			</div>
			<div class="col-sm-10">
				<button id="toggle" type="button" class="btn btn-default">show/hide password</button>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button name="update" type="submit" class="btn btn-default">update</button>
			</div>
		</div>
	</form>

	<script>
		$('#toggle').click(function(){
			$('#password').togglePassword();
		});
	</script>

</body>
</html>