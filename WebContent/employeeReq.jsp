<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>request form</title>
</head>
<body>

	<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="employeeHome.jsp"> <img alt="Brand"
				src="images/companyName.png" style="height: 63px; width: 121px"></a>
		</div>

		<p class="navbar-text navbar-right">
			current user: <a href="#" class="navbar-link">${user.username}</a>
		</p>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="employeeHome.jsp">home</a></li>
				<li class="active"><a href="employeeReq.jsp">submit a
						request</a></li>
				<li><a href="employeeInfo.jsp">account</a></li>
				<li><a href="logoutSubmit">log out</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid --> </nav>

	<div
		style="background-color: #f8f8f8; text-align: center; font-size: 16px">
		<b>submit a request</b>
	</div>
	<form name="request" id="form1" action="reimburseReq" method="post"
		enctype="multipart/form-data">
		<div class="form-group">
			<div class="col-sm-10">
				<label for="amount">amount:</label> <input type="text" name="amount"
					class="form-control numeric-only" id="amount"
					placeholder="desired reimbursement">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10">
				<label for="description">description:</label> <input type="text"
					name="description" class="form-control" id="description"
					placeholder="(optional)">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10">
				<label for="rtype">expense type:</label> <select name="rtype"
					class="form-control" id="rtype">
					<option value="" selected disabled hidden>choose an
						option...</option>
					<option value="1">travel</option>
					<option value="2">business</option>
					<option value="3">medical</option>
					<option value="4">other</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-10">
				<label for="receipt">receipt:</label> <input type="file"
					class="form-control-file" id="receipt" name="receipt"
					accept="image/*" aria-describedby="fileHelp">
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button id="btnSubmit" name="btnSubmit" type="submit"
					class="btn btn-default">submit</button>
			</div>
		</div>
	</form>

	<script>
		$(document).on('keyup', '.numeric-only', function(event) {
			var v = this.value;
			if ($.isNumeric(v) === false) {
				//chop off the last char entered
				this.value = this.value.slice(0, -1);
			}
		});

		$("#btnSubmit").on("click", function(e) {
			if ($("input[id=test]").is(":checked")) {
				check = $("#mycheckbox").prop("checked");
				if (!check) {
					alert("Please confirm");
					e.preventDefault(); // add this line
				}
			}
		});
	</script>
</body>
</html>