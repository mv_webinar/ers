<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<style>
form {
	margin: 0 auto;
	width: 500px;
	text-align: center;
}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>login</title>
</head>
<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Expense Reimbursement System</h1>
			<form action=loginSubmit method="post" class="form-inline"
				role="form">
				<div class="form-group">
					<label class="sr-only" for="username">username</label><input
						type="text" class="form-control" name="username" id="username"
						placeholder="enter username">
				</div>
				<div class="form-group">
					<label class="sr-only" for="password">password</label><input
						type="password" class="form-control" name="password" id="password"
						placeholder="password">
				</div>
				<button type="submit" class="btn btn-default" name="btnLogin">submit</button>
			</form>
		</div>
	</div>
</body>
</html>