package com.revature.bus;

import java.util.ArrayList;

import com.revature.beans.ErsRequest;

/**
 * 
 * @author daosyn
 *
 * class that handles all of the different filter-by options
 * that the admin can use
 * 
 */
public class Filters {

	/**
	 * 
	 * @param uid
	 * @param requests
	 * @return html table with requests from specified employee
	 * 
	 */
	public static String efilter(int uid, ArrayList<ErsRequest> requests) {
		String rows = "<thead><tr>"
				+ "<th>id</th>"
				+ "<th>amount</th>"
				+ "<th>description</th>"
				+ "<th>submitted</th>"
				+ "<th>resolved</th>"
				+ "<th>resolver</th>"
				+ "<th>type</th>"
				+ "<th>status</th>"
				+ "<th></th>"
				+ "<th></th>"
				+ "<th></th>"
				+ "</tr></thead>";
			
			rows += "<tbody>";
			for(ErsRequest request : requests){
				if(request.getUidAuthor() == uid){
					rows += "<tr>";
					
					rows += "<td>" + request.getRid() + "</td>";
					rows += "<td>$" + request.getAmount() + "</td>";
					rows += "<td>" + request.getDescription() + "</td>";
					rows += "<td>" + request.getSubmitted() + "</td>";
					rows += "<td>" + request.getResolved() + "</td>";
					rows += "<td>" + request.getResolver() + "</td>";
					rows += "<td>" + request.getType() + "</td>";
					rows += "<td>" + request.getStatus() + "</td>";
					
					//create buttons
					rows += "<td><a href='data:image/jpg;base64, " 
							+ request.getReceipt() 
							+ "' data-lightbox='receipt'>"
							+ "<button type='button' class='btn btn-default'>view receipt</button></a></td>"
							
							//approve request button
							+ "<td><form action='resolveReq' method='post' target='goHere'>"
							+ "<input type='hidden' name='rstatus' value='1' />"
							+ "<input type='hidden' name='rid' value='" + request.getRid() + "' />"
							+ "<input type='hidden' name='eid' value='" + request.getUidAuthor() + "' />"
							+ "<button type='submit' class='btn btn-default'>approve</button>"
							+ "</form></td>"
							
							//deny request button
							+ "<td><form action='resolveReq' method='post' target='goHere'>"
							+ "<input type='hidden' name='rstatus' value='2' />"
							+ "<input type='hidden' name='rid' value='" + request.getRid() + "' />"
							+ "<input type='hidden' name='eid' value='" + request.getUidAuthor() + "' />"
							+ "<button type='submit' class='btn btn-default'>deny</button>"
							+ "</form></td>";
								
					//end row
					rows += "</tr>";
				}

				//end table body
				rows += "<tbody>";
			}
			return rows;
	}

	/**
	 * 
	 * @param requests
	 * @return list of pending requests
	 * 
	 */
	public static ArrayList<ErsRequest> pfilter(ArrayList<ErsRequest> requests) {
		ArrayList<ErsRequest> pendingRequests = new ArrayList<>();
		for (ErsRequest request : requests){
			if(request.getStatus().equals("pending")){
				pendingRequests.add(request);
			}
		}
		return pendingRequests;
	}

	/**
	 * 
	 * @param requests
	 * @return list of resolved requests
	 * 
	 */
	public static ArrayList<ErsRequest> rfilter(ArrayList<ErsRequest> requests) {
		ArrayList<ErsRequest> resolvedRequests = new ArrayList<>();
		for (ErsRequest request : requests){
			if(request.getStatus().equals("approved") || request.getStatus().equals("denied")){
				resolvedRequests.add(request);
			}
		}
		return resolvedRequests;
	}

}
