package com.revature.bus;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.revature.beans.ErsRequest;
import com.revature.beans.ErsUser;

import oracle.jdbc.OracleTypes;

public class DAO {
	public static Connection conn;
	public static DAO dao;
	public static Context ctx;

	private DAO() {

		// Add two properties to a Properties object and pass to Context object
		Properties prop = new Properties();

		// Sets up the factory object for connection (key value pair)
		prop.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");

		// Provide the ip address and port to the WebLogic Server
		prop.put(Context.PROVIDER_URL, "t3://localhost:7001");

		try {
			// pass property parameter to Context object
			ctx = new InitialContext(prop);

			// call the JNDI name for the data source created in the WebLogic
			// Server
			DataSource ds = (DataSource) ctx.lookup("ERS");

			// make the connection
			conn = ds.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static DAO getDAO() {
		if (conn == null) {
			dao = new DAO();
		}
		return dao;
	}

	public static void closeConnection() {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
	public int getAuth(String username, String password) {
		// returns -1 if cs fails, 0 if credentials do not match
		int uid = -1;

		try {
			String auth = "{call SYS.AUTH(?, ?, ?)}";
			CallableStatement cs = conn.prepareCall(auth);
			cs.setString(1, username);
			cs.setString(2, password);
			cs.registerOutParameter(3, OracleTypes.NUMBER);

			cs.execute();
			uid = cs.getInt(3);
			cs.close();
		} catch (Exception e) {
			System.out.println("user not found");
		}
		return uid;
	}

	public ErsUser createErsUser(int uid) {
		
		ErsUser ersUser = new ErsUser();
		ersUser.setUid(uid);
		try {
			String selectUser = "{call SYS.SELECTUSER(?, ?)}";
			CallableStatement cs = conn.prepareCall(selectUser);
			cs.setInt(1, uid);
			cs.registerOutParameter(2, OracleTypes.CURSOR);

			cs.execute();
			ResultSet rs = (ResultSet) cs.getObject(2);
			
			while (rs.next()) {
				ersUser.setUsername(rs.getString("U_USERNAME"));
				ersUser.setPassword(rs.getString("U_PASSWORD"));
				ersUser.setFname(rs.getString("U_FIRSTNAME"));
				ersUser.setLname(rs.getString("U_LASTNAME"));
				ersUser.setEmail(rs.getString("U_EMAIL"));
				ersUser.setRole(rs.getInt("UR_ID"));
			}
			rs.close();
			cs.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ersUser;
	}

	public void updateUser(ErsUser ersUser) {
		try {
			String update = "{call SYS.UPDATEUSER(?, ?, ?, ?, ?)}";
			CallableStatement cs = conn.prepareCall(update);
			cs.setInt(1, ersUser.getUid());
			cs.setString(2, ersUser.getPassword());
			cs.setString(3, ersUser.getFname());
			cs.setString(4, ersUser.getLname());
			cs.setString(5, ersUser.getEmail());

			cs.execute();
			cs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void submitRequest(double amount, String description, byte[] receipt, int uidAuthor, int rtype) {
		try {
			Blob blob = conn.createBlob();
			blob.setBytes(1, receipt);

			String createReq = "{call SYS.CREATEREQ(?, ?, ?, ?, ?)}";
			CallableStatement cs = conn.prepareCall(createReq);
			cs.setDouble(1, amount);
			cs.setString(2, description);
			cs.setBlob(3, blob);
			cs.setInt(4, uidAuthor);
			cs.setInt(5, rtype);

			cs.execute();
			cs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * methods used to return lists of requests no args method returns list of
	 * all requests
	 */
	public ArrayList<ErsRequest> getRequests() {
		ArrayList<ErsRequest> ersRequests = new ArrayList<>();

		try {
			String getUserReqs = "{call SYS.GETALLREQS(?)}";
			CallableStatement cs = conn.prepareCall(getUserReqs);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();

			ResultSet rs = (ResultSet) cs.getObject(1);
			while (rs.next()) {
				ErsRequest ersRequest = new ErsRequest();

				// store fields into request object
				ersRequest.setRid(rs.getInt("R_ID"));
				ersRequest.setAmount(rs.getDouble("R_AMOUNT"));
				ersRequest.setDescription(rs.getString("R_DESCRIPTION"));
				ersRequest.setReceipt(rs.getBlob("R_RECEIPT"));
				ersRequest.setSubmitted(rs.getTimestamp("R_SUBMITTED"));
				ersRequest.setResolved(rs.getTimestamp("R_RESOLVED"));
				ersRequest.setUidAuthor(rs.getInt("U_ID_AUTHOR"));
				ersRequest.setUidResolver(rs.getInt("U_ID_RESOLVER"));
				ersRequest.setRtype(rs.getInt("RT_TYPE"));
				ersRequest.setRstatus(rs.getInt("RS_STATUS"));

				// add request to list
				ersRequests.add(ersRequest);
			}

			rs.close();
			cs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ersRequests;
	}

	/*
	 * returns list of requests that belong to a specific user
	 */
	public ArrayList<ErsRequest> getRequests(int uid) {
		ArrayList<ErsRequest> ersRequests = new ArrayList<>();

		try {
			String getUserReqs = "{call SYS.GETUSERREQS(?, ?)}";
			CallableStatement cs = conn.prepareCall(getUserReqs);
			cs.setInt(1, uid);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();

			ResultSet rs = (ResultSet) cs.getObject(2);
			while (rs.next()) {
				ErsRequest ersRequest = new ErsRequest();

				// store fields into request object
				ersRequest.setRid(rs.getInt("R_ID"));
				ersRequest.setAmount(rs.getDouble("R_AMOUNT"));
				ersRequest.setDescription(rs.getString("R_DESCRIPTION"));
				ersRequest.setReceipt(rs.getBlob("R_RECEIPT"));
				ersRequest.setSubmitted(rs.getTimestamp("R_SUBMITTED"));
				ersRequest.setResolved(rs.getTimestamp("R_RESOLVED"));
				ersRequest.setUidAuthor(rs.getInt("U_ID_AUTHOR"));
				ersRequest.setUidResolver(rs.getInt("U_ID_RESOLVER"));
				ersRequest.setRtype(rs.getInt("RT_TYPE"));
				ersRequest.setRstatus(rs.getInt("RS_STATUS"));

				// add request to list
				ersRequests.add(ersRequest);
			}

			rs.close();
			cs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ersRequests;
	}

	public void resolveReq(int manager, int uid, int rid, int rstatus) {
		try {
			String update = "{call SYS.RESOLVEREQ(?, ?, ?, ?)}";
			CallableStatement cs = conn.prepareCall(update);
			cs.setInt(1, manager);
			cs.setInt(2, uid);
			cs.setInt(3, rid);
			cs.setInt(4, rstatus);

			cs.execute();
			cs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<ErsUser> getEmpList() {
		try {
			String update = "{call SYS.GETALLEMPLOYEES(?)}";
			CallableStatement cs = conn.prepareCall(update);

			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();

			ResultSet rs = (ResultSet) cs.getObject(1);

			ArrayList<ErsUser> empList = new ArrayList<>();
			while (rs.next()) {
				if (rs.getInt("UR_ID") == 2) {
					ErsUser emp = new ErsUser();
					emp.setUid(rs.getInt("U_ID"));
					emp.setUsername(rs.getString("U_USERNAME"));
					emp.setPassword(rs.getString("U_PASSWORD"));
					emp.setFname(rs.getString("U_FIRSTNAME"));
					emp.setLname(rs.getString("U_LASTNAME"));
					emp.setEmail(rs.getString("U_EMAIL"));
					emp.setRole(2);

					empList.add(emp);
				}
			}
			cs.close();

			return empList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getUsername(int uid) {
		String username = null;

		if (uid == 0) {
			// no resolver
			return "";
		}
		try {
			String getUsername = "{call SYS.GETUSERNAME(?, ?)}";
			CallableStatement cs = conn.prepareCall(getUsername);
			cs.setInt(1, uid);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);

			cs.execute();
			username = cs.getString(2);

			cs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return username;
	}

	public String getEmail(int uid) {
		String email = null;
		
		try {
			String getEmail = "{call SYS.GETEMAIL(?, ?)}";
			CallableStatement cs = conn.prepareCall(getEmail);
			cs.setInt(1, uid);
			cs.registerOutParameter(2, OracleTypes.VARCHAR);

			cs.execute();
			email = cs.getString(2);

			cs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return email;
	}

	public void registerEmployee(String username, String fname, String lname, String email) {
		try {
			String register = "{call SYS.CREATEUSER(?, ?, ?, ?, ?)}";
			CallableStatement cs = conn.prepareCall(register);
			cs.setString(1, username);
			cs.setString(2, "temp");
			cs.setString(3, fname);
			cs.setString(4, lname);
			cs.setString(5, email);

			cs.execute();
			cs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
