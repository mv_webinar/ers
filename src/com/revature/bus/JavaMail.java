package com.revature.bus;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class JavaMail {
	public static void requestResolved(int uid) {
		DAO dao = DAO.getDAO();
		String email = dao.getEmail(uid);
		if (email == null) {
			return;
		}
		
		String subject = "reimbursement resolved";
		String text = "hi, this is an automated message to let "
				+ "you know that one of your reimbursement requests has been resolved";
		sendMessage(email, subject, text);
	}
	
	public static void registeredEmployee(String email, String username, String password) {
		String subject = "welcome to the ERS";
		String text = "hi, your username is " + username + " and password is " + password;
		sendMessage(email, subject, text);
	}

	public static void sendMessage(String email, String subject, String text) {
		// Sender's email ID needs to be mentioned
		String from = "ADMIN";
		final String username = "revature.ers.manager@gmail.com";
		final String password = "thisisatest";

		// Assuming you are sending email through relay.jangosmtp.net
		String host = "smtp.gmail.com";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "587");

		// Get the Session object.
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));

			// Set Subject: header field
			message.setSubject(subject);

			// Now set the actual message
			message.setText(text);

			// Send message
			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}
