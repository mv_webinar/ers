package com.revature.beans;

import com.revature.bus.DAO;

public class ErsUser {
	private int uid;
	private String username;
	private String password;
	private String fname;
	private String lname;
	private String email;
	private String role;

	public ErsUser() {
		super();
	}

/*	public ErsUser(int uid) {
		this();
		this.uid = uid;

		DAO dao = DAO.getDAO();
		dao.createErsUser(this);
	}*/

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(int role) {
		if(role == 1){
			this.role = "Manager";
		}
		else if(role == 2){
			this.role = "Employee";
		}
	}
}
