package com.revature.beans;

import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.revature.bus.DAO;
import com.sun.org.apache.xml.internal.security.utils.Base64;

public class ErsRequest {
	private int rid;
	private double amount;
	private String description;
	private String receipt;
	private Timestamp submitted;
	private Timestamp resolved;
	private int uidAuthor;
	private int uidResolver;
	private int rtype;
	private int rstatus;
	private String type;
	private String status;
	private String author;
	private String resolver;

	private static final String noReceipt = "iVBORw0KGgoAAAANSUhEUgAAAQ8AAABOCAYAAAA+XJNDAAAAAXNSR0"
			+ "IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAAS0SURBVHhe7d"
			+ "gBbqswEEXRbisbYjvZTTbTxVAacDD22HgeJg3VPZKlr4QOxh6/oP81AoCA8AAgITwASAgPAB"
			+ "LCA4CE8AAgITwASAgPABLCA4CE8AAgITwASAgPABLCA4CE8AAgITwASAgPABLCA4CE8AAgIT"
			+ "wASAgPABLCA4CE8AAgITwASAgPABLCA4CE8AAgITwASAgPABLCA4CE8Li0xzh8fY1f0xgey0"
			+ "fAmxAeF/Z9vz2D4zlIj8X3eL8taxKNT1yex/A7t9t4/14+6Ozs+oTHpZ345vEY5oN3uVAiPA"
			+ "LCA3/jsuGxFd7OCI/+CA/YCI/TER57lia8PZ9gfc2exzB9UhCaNxpzjaOW19rbffrXJL1Pqc"
			+ "u+7+Mtvm4auw1pPEPxb5rrO1/LPetvzNcah/Yh3MOYsHnQPfM3NIeH8ey159z8f1PD9fncwy"
			+ "gcbne/Oet38L7wGIZsMZ4jHOLInJiFYVzvs4bHvXCfrAlqh8rcUfuAz8PYTFd9MTxK6x8fwN"
			+ "o8ovEn4dEyf0NLeHj7rXq9daPquh7th4m3fidvC4/8QUJSJg/3uj75PEriWiPsSw5fXCzce9"
			+ "MwYZ5pkxbmP1l/lfLvHkP6mb9+bPdwFNd/XQczDMLfHVvsXKVuLTzm4Zj/on19knWu9NtveO"
			+ "T3LOzXTp18f5394K7fzxvDI3+IkODxQ1ufvYRah94+1qbL7xG+Wzdubr7CBiwbt22ktkMf+O"
			+ "tvtR8O4xc6fGf9ce27Iyp16+HhnP9ib3169pt1WMP9rT0sX9/eD976Pb0vPJqapZS6wd73Lf"
			+ "KAqAnNVR3xs4VfglK3Jtz1E3uHo7b+r/W0DkfDwZS4+mGizn9RXx+938r7tj2stQNsfeftB2"
			+ "/9nj4zPIrN4Dv4Nk+N9S2lOuJnW5639rawEuonCI/C/BdN4eHqt709iw9rvdfyw+3tB2/9vq"
			+ "755lFpln31BU+5N8B56I5u8KHwCG9JVw2P2vwXTeHh6bcwH+NvymFg76+1975+8Nfv6cPCY0"
			+ "1ec7O7NLQvPMIc294kJqGhz6qfqB+OScP6m/fustaGYt1wkNvDo2Xt6uvj77faPcthYNR/9c"
			+ "n2+pZninnr9/Rh4bF+lj30azFKjdDKFx7xfb0bav86lZ+rtX6sfjgmpfUPn5ea66zms+pGa5"
			+ "A9izr/xd76ePvtdf3mbWcNvqxOmKf59mJc7+0Hb/2OPi48pk9fvwbWUA7YljM8JmuDWcPanL"
			+ "iZ0pFf76qfHDRrbNZo00j5KK9neR+O7UG57jDMc930g3f+3vXx9ttu/XR/S70wjPfnvh/sB6"
			+ "F+Lx8YHrN8AXstgj88nopNU57X+gayDutZn1rrdwuPtue3nuFYePxKD+wyl2WubeFRmL87PG"
			+ "aufsvuMc9lrmH9XXLAlwcsXz9x9ZtQv4PzwwN/qxLel3D1+f9jhMd/R3jgJITHf0d44CSEx3"
			+ "9HeOAkhAcACeEBQEJ4AJAQHgAkhAcACeEBQEJ4AJAQHgAkhAcACeEBQEJ4AJAQHgAkhAcACe"
			+ "EBQEJ4AJAQHgAkhAcACeEBQEJ4AJAQHgAkhAcACeEBQEJ4AJAQHgAkhAcACeEBQEJ4AJAQHg"
			+ "AkhAcAwTj+ABzHheuTq1/VAAAAAElFTkSuQmCC";
	
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		if(description == null){
			description = "";
		}
		
		this.description = description;
	}
	public String getReceipt() {
		return receipt;
	}
	public void setReceipt(Blob blob) {
		String receipt = null;
		
		try {
			byte[] ba;
			ba = blob.getBytes(1, (int) blob.length());
			receipt = Base64.encode(ba);
			if (receipt.equals("")){
				receipt = noReceipt;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		this.receipt = receipt;
	}
	public Timestamp getSubmitted() {
		return submitted;
	}
	public void setSubmitted(Timestamp submitted) {
		this.submitted = submitted;
	}
	public Timestamp getResolved() {
		return resolved;
	}
	public void setResolved(Timestamp resolved) {
		this.resolved = resolved;
	}
	public int getUidAuthor() {
		return uidAuthor;
	}
	public void setUidAuthor(int uidAuthor) {
		DAO dao = DAO.getDAO();
		setAuthor(dao.getUsername(uidAuthor));
		this.uidAuthor = uidAuthor;
	}
	public int getUidResolver() {
		return uidResolver;
	}
	public void setUidResolver(int uidResolver) {
		DAO dao = DAO.getDAO();
		setResolver(dao.getUsername(uidResolver));
		this.uidResolver = uidResolver;
	}
	public int getRtype() {
		return rtype;
	}
	public void setRtype(int rtype) {
		switch(rtype){
		case 1:
			setType("travel");
			break;
		case 2:
			setType("business");
			break;
		case 3:
			setType("medical");
			break;
		case 4:
			setType("other");
			break;
		default:
			break;
		}
		this.rtype = rtype;
	}
	public int getRstatus() {
		return rstatus;
	}
	public void setRstatus(int rstatus) {
		if(rstatus == 0){
			setStatus("pending");
		}
		else if(rstatus == 1){
			setStatus("approved");
		}

		else if(rstatus == 2){
			setStatus("denied");
		}
		this.rstatus = rstatus;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getResolver() {
		return resolver;
	}
	public void setResolver(String resolver) {
		this.resolver = resolver;
	}
}
