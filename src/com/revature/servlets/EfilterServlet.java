package com.revature.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.revature.beans.ErsRequest;
import com.revature.bus.Filters;

/**
 * Servlet implementation class EfilterServlet
 */
@WebServlet("/EfilterServlet")
public class EfilterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		session.setAttribute("manPage", "filterEmployee.jsp");
		
		ArrayList<ErsRequest> requests = (ArrayList<ErsRequest>) session.getAttribute("requests");
		
		int uid = Integer.parseInt(req.getParameter("uid"));
		
		resp.setContentType("html");
		resp.getWriter().println(Filters.efilter(uid, requests));
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
