package com.revature.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.revature.beans.ErsUser;

@WebServlet("/EmployeeJSONServlet")
public class EmployeeJSONServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		@SuppressWarnings("unchecked")
		ArrayList<ErsUser> empList = (ArrayList<ErsUser>) session.getAttribute("employees");
		
		//some JSON bs
		Gson gson = new Gson();
		JsonElement element = gson.toJsonTree(empList, new TypeToken<List<ErsUser>>() {}.getType());
		JsonArray ja = element.getAsJsonArray();
		resp.setContentType("application/json");
		resp.getWriter().print(ja);
	}

	
	
}
