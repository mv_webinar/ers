package com.revature.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;

import com.revature.beans.ErsRequest;
import com.revature.beans.ErsUser;
import com.revature.bus.DAO;

@MultipartConfig
public class RequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//get receipt and convert to byte array
		HttpSession session = req.getSession();
		Part filePart = req.getPart("receipt");
		InputStream fileContent = filePart.getInputStream();
		byte[] receipt = IOUtils.toByteArray(fileContent);

		//get other input parameters
		try{
			final double amount = Double.parseDouble(req.getParameter("amount"));
			final String description = req.getParameter("description");
			final int rtype = Integer.parseInt(req.getParameter("rtype"));
			
			//grab id from session user
			ErsUser ersUser = (ErsUser)session.getAttribute("user");		
			int uidAuthor = ersUser.getUid();
			
			//submit request to database
			DAO dao = DAO.getDAO();
			dao.submitRequest(amount, description, receipt, uidAuthor, rtype);
			
			//reload requests
			ArrayList<ErsRequest> requests = dao.getRequests(uidAuthor);
			session.setAttribute("requests", requests);
		}catch(Exception e){
			resp.sendRedirect("employeeReq.jsp");
			return;
		}
		
		//forward to next page
		String next = "employeeHome.jsp";
		resp.sendRedirect(next);
	}
}
