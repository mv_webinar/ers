package com.revature.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.revature.beans.ErsRequest;
import com.revature.bus.Filters;

public class PfilterServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		ArrayList<ErsRequest> requests = (ArrayList<ErsRequest>) session.getAttribute("requests");
		
		session.setAttribute("pendingRequests", Filters.pfilter(requests));
		
		String next = "filterPending.jsp";
		session.setAttribute("manPage", next);
		
		RequestDispatcher rd = req.getRequestDispatcher(next);
		rd.forward(req, resp);
	}
}
