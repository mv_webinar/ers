package com.revature.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.revature.beans.ErsUser;
import com.revature.bus.DAO;

public class UpdateServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String password = req.getParameter("password");
		String fname = req.getParameter("fname");
		String lname = req.getParameter("lname");
		String email = req.getParameter("email");
		
		HttpSession session = req.getSession();
		ErsUser ersUser = (ErsUser)session.getAttribute("user");
		
		if(!password.equals("")) ersUser.setPassword(password);
		if(!fname.equals("")) ersUser.setFname(fname);
		if(!lname.equals("")) ersUser.setLname(lname);
		if(!email.equals("")) ersUser.setEmail(email);
		
		DAO dao = DAO.getDAO();
		dao.updateUser(ersUser);

		//get rid of session objects
		session.invalidate();
		System.gc();
		
		String next = "index.html";
		RequestDispatcher rd = req.getRequestDispatcher(next);
		rd.forward(req, resp);
	}
}
