package com.revature.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.revature.beans.ErsRequest;
import com.revature.beans.ErsUser;
import com.revature.bus.DAO;
import com.revature.bus.Filters;
import com.revature.bus.JavaMail;

public class ResolverServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		if(!session.getAttribute("role").equals("Manager")){
			System.out.println("you don't belong here!");

			resp.sendRedirect("employeeHome.jsp");
			return;
		}
		
		ErsUser ersUser = (ErsUser) session.getAttribute("user");
		
		int uid = Integer.parseInt(req.getParameter("eid"));
		int rid = Integer.parseInt(req.getParameter("rid"));
		int rstatus = Integer.parseInt(req.getParameter("rstatus"));
		
		int manager = ersUser.getUid();
		
		DAO dao = DAO.getDAO();
		dao.resolveReq(manager, uid, rid, rstatus);
		JavaMail.requestResolved(uid);
		
		//refresh lists of requests
		ArrayList<ErsRequest> requests = dao.getRequests();
		session.setAttribute("requests", requests);
		session.setAttribute("pendingRequests", Filters.pfilter(requests));
		session.setAttribute("resolvedRequests", Filters.rfilter(requests));
		
		//forward to next page
		String next = (String) session.getAttribute("manPage");
		RequestDispatcher rd = req.getRequestDispatcher(next);
		rd.forward(req, resp);
	}
	
}
