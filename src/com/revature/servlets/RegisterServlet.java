package com.revature.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.revature.bus.DAO;
import com.revature.bus.JavaMail;

public class RegisterServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String username = req.getParameter("username");
		String fname = req.getParameter("fname");
		String lname = req.getParameter("lname");
		String email = req.getParameter("email");
		
		DAO dao = DAO.getDAO();
		dao.registerEmployee(username, fname, lname, email);
		JavaMail.registeredEmployee(email, username, "temp");

		String next = "managerHome.jsp";
		RequestDispatcher rd = req.getRequestDispatcher(next);
		rd.forward(req, resp);
	}
	
}
