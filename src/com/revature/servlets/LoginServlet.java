package com.revature.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.revature.beans.ErsRequest;
import com.revature.beans.ErsUser;
import com.revature.bus.DAO;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.gc();
		System.out.println("username");
		final String username = req.getParameter("username");
		final String password = req.getParameter("password");
		
		//create DAO and authenticate user
		DAO dao = DAO.getDAO();
		System.out.println("username");
		int uid = dao.getAuth(username, password);
		System.out.println(uid);
		String next;
		HttpSession session = req.getSession();
		if (uid <= 0){
			next = "login.jsp";
			RequestDispatcher rd = req.getRequestDispatcher(next);
			rd.forward(req, resp);
		}
		else {
			/*ErsUser ersUser = new ErsUser(uid);*/
			ErsUser ersUser = dao.createErsUser(uid);
			System.out.println(uid + "x");
			session.setAttribute("user", ersUser);
			session.setAttribute("role", ersUser.getRole());
			
			if (ersUser.getRole().equals("Manager")){
				next = "managerHome.jsp";
				session.setAttribute("manPage", next);

				ArrayList<ErsRequest> ersRequests = dao.getRequests();
				session.setAttribute("requests", ersRequests);
				
				ArrayList<ErsUser> empList = dao.getEmpList();
				session.setAttribute("employees", empList);
			}
			else if (ersUser.getRole().equals("Employee")){
				next = "employeeHome.jsp";
				
				ArrayList<ErsRequest> ersRequests = dao.getRequests(uid);
				session.setAttribute("requests", ersRequests);
			}
			else{
				next = "login.jsp";
			}

			//forward to next page
			RequestDispatcher rd = req.getRequestDispatcher(next);
			rd.forward(req, resp);
		}
		
	}
}
