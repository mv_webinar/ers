package com.revature.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FilterPendingPage {
	public FilterPendingPage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	public WebElement btnApprove(WebDriver driver){
		return driver.findElement(By.xpath("/html/body/table/tbody/tr[1]/td[9]/form/button"));
	}
	
	@FindBy(name = "dropdown")
	public WebElement dropdown;
	
	@FindBy(name = "fres")
	public WebElement btnRfilter;
}
