package com.revature.tests;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ErsTests {
	private static WebDriver driver = null;

	@Test(priority = 0)
	public void launch() throws InterruptedException {
		IndexPage iPage = new IndexPage(driver);

		Thread.sleep(500);

		iPage.btnIndex.click();
	}

	@Test(priority = 1)
	public void login() throws InterruptedException {
		LoginPage lPage = new LoginPage(driver);
		lPage.username.sendKeys("test");
		lPage.password.sendKeys("test");

		Thread.sleep(1500);

		lPage.btnLogin.click();
	}

	@Test(priority = 2)
	public void request() throws InterruptedException {

		Thread.sleep(3000);

		EmployeeHomePage ehPage = new EmployeeHomePage(driver);
		ehPage.btnReq.click();

		EmployeeReqPage erPage = new EmployeeReqPage(driver);
		erPage.amount.sendKeys("100.99");
		erPage.description.sendKeys("this is a test");

		Select select = new Select(erPage.rtype);
		select.selectByValue("4");

		erPage.receipt.sendKeys("C:\\Users\\Ankit\\Documents\\workspace-sts-3.8.3.RELEASE\\ExpenseReimbursementSystem\\WebContent\\mockReceipts\\rec2.jpg");

		Thread.sleep(1500);

		erPage.btnSubmit.click();

	}

	@Test(priority = 3)
	public void changeInfo() throws InterruptedException {
		EmployeeHomePage ehPage = new EmployeeHomePage(driver);
		ehPage.btnAccount.click();

		EmployeeInfoPage eiPage = new EmployeeInfoPage(driver);
		eiPage.fname.clear();
		eiPage.fname.sendKeys("Test");
		eiPage.lname.clear();
		eiPage.lname.sendKeys("Test");
		eiPage.email.clear();
		eiPage.email.sendKeys("elmobey@gmail.com");
		eiPage.password.sendKeys("CanYouSeeThis?");

		Thread.sleep(1000);

		eiPage.toggle.click();

		Thread.sleep(2500);

		eiPage.toggle.click();

		eiPage.password.clear();
		eiPage.password.sendKeys("test");
		eiPage.update.click();
	}

	@Test(priority = 4)
	public void viewReceipt() throws InterruptedException {
		launch();
		login();
		EmployeeHomePage ehPage = new EmployeeHomePage(driver);
		ehPage.viewReceipt(driver).click();

		Thread.sleep(4000);

		driver.findElement(By.className("lb-close")).click();
	}

	@Test(priority = 5)
	public void logoutEmp() throws InterruptedException {

		Thread.sleep(1500);

		EmployeeHomePage ehPage = new EmployeeHomePage(driver);
		ehPage.btnLogout.click();
	}

	@Test(priority = 6)
	public void loginMan() throws InterruptedException {
		launch();
		LoginPage lPage = new LoginPage(driver);
		lPage.username.sendKeys("ADMIN");
		lPage.password.sendKeys("ADMIN");

		Thread.sleep(1500);

		lPage.btnLogin.click();

		Thread.sleep(4500);
	}

	@Test(priority = 7)
	public void filterEmployee() throws InterruptedException {
		ManagerHomePage mhPage = new ManagerHomePage(driver);
		mhPage.dropdown.click();

		Thread.sleep(500);

		mhPage.btnEfilter.click();

		FilterEmployeePage fePage = new FilterEmployeePage(driver);
		Select select = new Select(fePage.empList);
		select.selectByValue("2");

		Thread.sleep(1000);

		select.selectByValue("10");

		Thread.sleep(1000);

		/*select.selectByValue("3");

		Thread.sleep(1000);

		select.selectByValue("1");

		Thread.sleep(1000);

		select.selectByValue("2");

		Thread.sleep(7000);*/

		fePage.dropdown.click();

		Thread.sleep(500);

		fePage.btnPfilter.click();

		Thread.sleep(3000);

	}

	@Test(priority = 8)
	public void filterPending() throws InterruptedException {
		FilterPendingPage fpPage = new FilterPendingPage(driver);
		fpPage.btnApprove(driver).click();

		Thread.sleep(2000);

		fpPage.dropdown.click();

		Thread.sleep(500);

		fpPage.btnRfilter.click();

		Thread.sleep(3000);

	}

	@Test(priority = 9)
	public void filterResolved() throws InterruptedException {
		FilterResolvedPage frPage = new FilterResolvedPage(driver);
		frPage.btnViewReceipt(driver).click();

		Thread.sleep(2000);

		driver.findElement(By.className("lb-next")).click();

		Thread.sleep(2000);

		driver.findElement(By.className("lb-next")).click();

		Thread.sleep(2000);

		driver.findElement(By.className("lb-next")).click();

		Thread.sleep(2000);

		driver.findElement(By.className("lb-close")).click();

		Thread.sleep(1000);

		frPage.btnLogout.click();
	}

	@Test(priority = 10)
	public void register() throws InterruptedException {
		loginMan();
		ManagerHomePage mhPage = new ManagerHomePage(driver);
		mhPage.btnRegister.click();

		RegisterEmployeePage rePage = new RegisterEmployeePage(driver);
		rePage.username.sendKeys("test4");
		rePage.fname.sendKeys("test");
		rePage.lname.sendKeys("test");
		rePage.email.sendKeys("tohe@stromox.com");

		Thread.sleep(1500);

		rePage.btnSubmit.click();

		Thread.sleep(1500);

		mhPage.btnLogout.click();
	}

	@Test(priority = 11)
	public void loginNew() throws InterruptedException {
		launch();
		LoginPage lPage = new LoginPage(driver);
		lPage.username.sendKeys("test4");
		lPage.password.sendKeys("temp");

		Thread.sleep(1500);

		lPage.btnLogin.click();

		Thread.sleep(4500);
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

	@BeforeTest
	public void beforeTest() {
		// load driver
		File file = new File("C:/selenium/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());

		// create instance of chrome driver
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://localhost:7001/ers");
	}
}
