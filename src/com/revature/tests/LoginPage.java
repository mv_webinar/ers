package com.revature.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	public LoginPage (WebDriver driver){
		PageFactory.initElements(driver, this);
	}

	@FindBy(name = "username")
	public WebElement username;

	@FindBy(name = "password")
	public WebElement password;
	
	@FindBy(name = "btnLogin")
	public WebElement btnLogin;
}
