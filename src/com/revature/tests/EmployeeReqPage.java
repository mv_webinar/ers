package com.revature.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EmployeeReqPage {
	public EmployeeReqPage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}

	@FindBy(name = "amount")
	public WebElement amount;

	@FindBy(name = "description")
	public WebElement description;

	@FindBy(name = "rtype")
	public WebElement rtype;
	
	@FindBy(name = "receipt")
	public WebElement receipt;

	@FindBy(name = "btnSubmit")
	public WebElement btnSubmit;
}
