package com.revature.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FilterEmployeePage {
	public FilterEmployeePage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}

	@FindBy(name = "empList")
	public WebElement empList;
	
	@FindBy(name = "dropdown")
	public WebElement dropdown;
	
	@FindBy(name = "fpend")
	public WebElement btnPfilter;
}
