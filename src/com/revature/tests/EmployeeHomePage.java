package com.revature.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EmployeeHomePage {
	public EmployeeHomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(name = "btnReq")
	public WebElement btnReq;
	
	@FindBy(name = "btnAccount")
	public WebElement btnAccount;
	
	public WebElement viewReceipt(WebDriver driver){
		return driver.findElement(By.xpath("/html/body/table/tbody/tr[1]/td[8]/a/button"));
	}
	
	@FindBy(name = "btnLogout")
	public WebElement btnLogout;
}
