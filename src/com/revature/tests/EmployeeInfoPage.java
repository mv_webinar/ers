package com.revature.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EmployeeInfoPage {
	public EmployeeInfoPage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}

	@FindBy(name = "fname")
	public WebElement fname;

	@FindBy(name = "lname")
	public WebElement lname;

	@FindBy(name = "email")
	public WebElement email;

	@FindBy(name = "password")
	public WebElement password;
	
	@FindBy(id = "toggle")
	public WebElement toggle;
	
	@FindBy(name = "update")
	public WebElement update;
}
