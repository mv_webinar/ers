package com.revature.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FilterResolvedPage {
	public FilterResolvedPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public WebElement btnViewReceipt(WebDriver driver) {
		return driver.findElement(By.xpath("/html/body/table/tbody/tr[1]/td[10]/a/button"));
	}
	
	@FindBy(name = "logout")
	public WebElement btnLogout;
}
