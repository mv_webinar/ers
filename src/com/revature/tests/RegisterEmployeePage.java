package com.revature.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegisterEmployeePage {

	public RegisterEmployeePage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name = "username")
	public WebElement username;

	@FindBy(name = "fname")
	public WebElement fname;

	@FindBy(name = "lname")
	public WebElement lname;
	
	@FindBy(name = "email")
	public WebElement email;
	
	@FindBy(name = "register")
	public WebElement btnSubmit;
}
