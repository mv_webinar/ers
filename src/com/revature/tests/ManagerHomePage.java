package com.revature.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManagerHomePage {
	public ManagerHomePage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name = "dropdown")
	public WebElement dropdown;

	@FindBy(name = "femploy")
	public WebElement btnEfilter;

	@FindBy(name = "regEmployee")
	public WebElement btnRegister;
	
	@FindBy(name = "logout")
	public WebElement btnLogout;
}
