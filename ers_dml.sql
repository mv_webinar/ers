--------------------------------------------------------
--  File created - Tuesday-January-10-2017   
--------------------------------------------------------
Insert into ERS_REIMBURSEMENT_STATUS (RS_ID,RS_STATUS) values (0,'pending');
Insert into ERS_REIMBURSEMENT_STATUS (RS_ID,RS_STATUS) values (1,'approved');
Insert into ERS_REIMBURSEMENT_STATUS (RS_ID,RS_STATUS) values (2,'denied');

Insert into ERS_REIMBURSEMENT_TYPE (RT_ID,RT_TYPE) values (1,'travel expense');
Insert into ERS_REIMBURSEMENT_TYPE (RT_ID,RT_TYPE) values (2,'business expense');
Insert into ERS_REIMBURSEMENT_TYPE (RT_ID,RT_TYPE) values (3,'medical expense');
Insert into ERS_REIMBURSEMENT_TYPE (RT_ID,RT_TYPE) values (4,'other');

Insert into ERS_USER_ROLES (UR_ID,UR_ROLE) values (1,'Manager');
Insert into ERS_USER_ROLES (UR_ID,UR_ROLE) values (2,'Employee');

Insert into ERS_USERS (U_ID,U_USERNAME,U_PASSWORD,U_FIRSTNAME,U_LASTNAME,U_EMAIL,UR_ID) values (9999,'manager','manager',null,null,null,1);
Insert into ERS_USERS (U_ID,U_USERNAME,U_PASSWORD,U_FIRSTNAME,U_LASTNAME,U_EMAIL,UR_ID) values (1337,'ADMIN','ADMIN',null,null,null,1);
Insert into ERS_USERS (U_ID,U_USERNAME,U_PASSWORD,U_FIRSTNAME,U_LASTNAME,U_EMAIL,UR_ID) values (2,'test','test','Test','Test','elmobey@gmail.com',2);
Insert into ERS_USERS (U_ID,U_USERNAME,U_PASSWORD,U_FIRSTNAME,U_LASTNAME,U_EMAIL,UR_ID) values (10,'test4','temp','test','test','tohe@stromox.com',2);
